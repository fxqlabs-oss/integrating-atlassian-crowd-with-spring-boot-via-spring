package net.fxqlabs.oss.blogs.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ControllerBundle {

    @RequestMapping("/")
    public ModelAndView getHomepage() {
        return new ModelAndView("index");
    }

    @RequestMapping("/users")
    public ModelAndView getUsersPage() {
        return new ModelAndView("users");
    }

    @RequestMapping("/managers")
    public ModelAndView getMembersPage() {
        return new ModelAndView("managers");
    }
}
