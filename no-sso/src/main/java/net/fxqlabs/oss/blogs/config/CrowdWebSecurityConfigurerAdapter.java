package net.fxqlabs.oss.blogs.config;

import com.atlassian.crowd.integration.http.CrowdHttpAuthenticator;
import com.atlassian.crowd.integration.http.CrowdHttpAuthenticatorImpl;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelper;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelperImpl;
import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractor;
import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractorImpl;
import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.integration.springsecurity.CrowdSSOAuthenticationDetails;
import com.atlassian.crowd.integration.springsecurity.RemoteCrowdAuthenticationProvider;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsServiceImpl;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.util.Properties;

@Configuration
@EnableWebSecurity
public class CrowdWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    @Value("${crowd.application.name}")
    private String applicationName;

    @Value("${crowd.application.password}")
    private String applicationPassword;

    @Value("${crowd.application.login.url:http://localhost:8080/login}")
    private String applicationLoginUrl;

    @Value("${crowd.server.url:http://localhost:8095/crowd/services/}")
    private String crowdServerUrl;

    @Value("${crowd.base.url:http://localhost:8095/crowd/}")
    private String crowdBaseUrl;

    @Value("${crowd.session.isauthenticated:session.isauthenticated")
    private String sessionIsauthenticated;

    @Value("${crowd.session.tokenkey:session.tokenkey}")
    private String sessionTokenkey;

    @Value("${crowd.session.validationinterval:2}")
    private String sessionValidationinterval;

    @Value("${crowd.session.lastvalidation:session.lastvalidation}")
    private String sessionLastvalidation;

    @Value("${crowd.cookie.tokenkey:crowd.token_key}")
    private String cookieTokenkey;

    @Bean
    ClientProperties clientProperties() {
        Properties crowdProperties = new Properties();
        crowdProperties.setProperty("application.name", applicationName);
        crowdProperties.setProperty("application.password", applicationPassword);
        crowdProperties.setProperty("application.login.url", applicationLoginUrl);
        crowdProperties.setProperty("crowd.server.url", crowdServerUrl);
        crowdProperties.setProperty("crowd.base.url", crowdBaseUrl);
        crowdProperties.setProperty("session.isauthenticated", sessionIsauthenticated);
        crowdProperties.setProperty("session.tokenkey", sessionTokenkey);
        crowdProperties.setProperty("session.validationinterval", sessionValidationinterval);
        crowdProperties.setProperty("session.lastvalidation", sessionLastvalidation);
        crowdProperties.setProperty("cookie.tokenkey", cookieTokenkey);
        return ClientPropertiesImpl.newInstanceFromProperties(crowdProperties);
    }

    @Bean
    public RestCrowdClientFactory crowdClientFactory() {
        return new RestCrowdClientFactory();
    }

    @Bean
    public CrowdClient crowdClient() {
        return crowdClientFactory().newInstance(clientProperties());
    }

    @Bean
    public CrowdHttpValidationFactorExtractor validationFactorExtractor() {
        return CrowdHttpValidationFactorExtractorImpl.getInstance();
    }

    @Bean
    public CrowdHttpTokenHelper tokenHelper() {
        return CrowdHttpTokenHelperImpl.getInstance(validationFactorExtractor());
    }

    @Bean
    public CrowdHttpAuthenticatorImpl crowdHttpAuthenticator() {
        return new CrowdHttpAuthenticatorImpl(crowdClient(), clientProperties(), tokenHelper());
    }

    @Bean
    public CrowdUserDetailsServiceImpl crowdUserDetailsService() {
        CrowdUserDetailsServiceImpl cuds = new CrowdUserDetailsServiceImpl();
        cuds.setCrowdClient(crowdClient());
        cuds.setAuthorityPrefix("ROLE_");
        return cuds;
    }

    @Bean
    public AuthenticationProvider crowdAuthenticationProvider() {
        return new MyRemoteCrowdAuthenticationProvider(crowdClient(), crowdHttpAuthenticator(), crowdUserDetailsService());
    }

    /**
     * Custom RemoteCrowdAuthenticationProvider to fix what appears to be a bug in the supports method
     */
    private class MyRemoteCrowdAuthenticationProvider extends RemoteCrowdAuthenticationProvider {

        public MyRemoteCrowdAuthenticationProvider(CrowdClient authenticationManager, CrowdHttpAuthenticator httpAuthenticator, CrowdUserDetailsService userDetailsService) {
            super(authenticationManager, httpAuthenticator, userDetailsService);
        }

        @Override
        public boolean supports(AbstractAuthenticationToken authenticationToken) {
            return authenticationToken.getDetails() != null || authenticationToken.getDetails() instanceof CrowdSSOAuthenticationDetails;
        }
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/users").hasRole("users")
                .antMatchers("/managers").hasRole("managers")
                .antMatchers("/").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().permitAll()
                .and()
                .logout().permitAll();
    }

}
