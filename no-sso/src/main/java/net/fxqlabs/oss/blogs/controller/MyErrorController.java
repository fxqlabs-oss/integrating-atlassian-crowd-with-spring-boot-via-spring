package net.fxqlabs.oss.blogs.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.NestedServletException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static javax.servlet.RequestDispatcher.ERROR_STATUS_CODE;

@Controller
public class MyErrorController implements ErrorController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyErrorController.class);

    @RequestMapping("/error")
    public ModelAndView handleError(HttpServletRequest request) {
        Map<String, Object> model = new HashMap<>();
        model.put("code", getHttpStatus(request));
        model.put("message", getErrorMessage(request));
        return new ModelAndView("error", model);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    private HttpStatus getHttpStatus(HttpServletRequest request) {
        return HttpStatus.resolve(
                Integer.parseInt(request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE).toString())
        );
    }

    private String getErrorMessage(HttpServletRequest request) {
        HttpStatus httpStatus = HttpStatus.resolve(
                Integer.parseInt(request.getAttribute(ERROR_STATUS_CODE).toString())
        );

        String message = httpStatus.getReasonPhrase() + ": Error " + httpStatus.value();

        try {
            switch (httpStatus) {
                case INTERNAL_SERVER_ERROR:
                    NestedServletException exception = (NestedServletException) request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
                    message = exception.getCause().getLocalizedMessage();
                    break;
            }
        } catch (Exception e) {
            LOGGER.error("Failed to populate the API Error with additional information", e);
        }
        return message;
    }
}
