package net.fxqlabs.oss.blogs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrowdSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrowdSecurityApplication.class, args);
    }

}
