# Integrating atlassian crowd with Spring Boot via Spring Security

To get started with this example first spin up crowd using docker

The crowd-img folder contains a dockerfile to download and build the image

Start it by opening a terminal in the project root and run:
```
docker-compose up -d
```

Head to http://localhost:8095/ and setup crowd using the embedded database.

Once logged into crowd add a new generic application with the following details:

- Name: springsecapp
- Password: password123
- URL: http://localhost:8080
- Remote IP Address: 127.0.0.1
- Directories: Tick "Crowd Internal Directory"
- Authorisation: Tick "Allow all users to authenticate"

Now start the Spring Boot application, the application.yml has config setup as above.

Head to http://localhost:8080/login

When you first attempt to login you'll get an error as your IP is not recognised by crowd.
Get the IP from the error message and add it to the crowd application's remote addresses.

Now try and log in, all should work ok!

When you now navigate to the Users tab, you will get a 403 forbidden error. This is because you are authenticated but not authorised.

/users is locked to members of the crowd group "users"
/managers is locked to members of the crowd group "managers"

Go ahead and create these groups in crowd, add you admin user as a member of the group, log out and back in and you will see these are now visible.